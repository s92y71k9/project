document.getElementById("hello_text").textContent = "初めてのJavaScript";
document.addEventListener("keydown",onkeydown);

var count = 0;
var cells;

//ブロックのパターン
var blocks = {
    i:{
        class: "i",
        pattern: [
            [1,1,1,1]
        ]
    },
    o:{
        class: "o",
        pattern: [
            [1,1],
            [1,1]
        ]
    },
    t:{
        class: "t",
        pattern: [
            [0,1,0],
            [1,1,1]
        ]
    },
    s:{
        class: "s",
        pattern: [
            [0,1,1],
            [1,1,0]
        ]
    },
    z:{
        class: "z",
        pattern: [
            [1,1,0],
            [0,1,1]
        ]
    },
    j:{
        class: "j",
        pattern: [
            [1,0,0],
            [1,1,1]
        ]
    },
    l:{
        class: "l",
        pattern: [
            [0,0,1],
            [1,1,1]
        ]
    }
}

loadTable()
setInterval(function(){
    //何回目かを数える
    count++;
    document.getElementById("hello_text").textContent="初めてのJavaScript("+count+")";
    /*ブロックが積み上がっていないかどうか
    for (var row = 0;row < 2;row++){
        for (var col = 0;col < 10;col++){
            if(cells[row][col].className !== ""){
                alert("game over!");
            }
        }
    }
    */
    
    if(hasFallingBlock()){          //落下中のブロックの有無の確認
        fallBlocks();               //ブロックを落とす
    }else{
        deleteRow();                //揃っている行を消す
        generateBlock();            //ランダムにブロックを生成する
    }
},1000);

function loadTable(){
    cells = [];
    var td_array = document.getElementsByTagName("td");
    var index = 0;
    //20*10の配列
    for(var row=0;row<20;row++){
        cells[row] = [];
        for(var col=0;col<10;col++){
            cells[row][col] = td_array[index];
            index++;
        }
    }
}

function fallBlocks(){
    //底についていないか
    for (var col=0;col<10;col++){
        if(cells[19][col].blockNum === fallingBlockNum){
            isFalling = false;
            return;
        }
    }
    //1マス下に別のブロックがないかどうか
    for (var row=18;row>=0;row--){
        for (var col=0;col<10;col++){
            if(cells[row][col].blockNum === fallingBlockNum){
                if(cells[row+1][col].className !== "" && cells[row+1][col].blockNum !== fallingBlockNum){
                    isFalling = false;
                    return;
                }
            }
        }
    }
    //下から二番目の行から繰り返しクラスを下げていく
    for (var row=18;row>=0;row--){
        for (var col=0;col<10;col++){
            if(cells[row][col].blockNum === fallingBlockNum){
                cells[row+1][col].className = cells[row][col].className;
                cells[row+1][col].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

var isFalling = false;
function hasFallingBlock(){
    return isFalling;
}

function onkeydown(event){
    if(event.keyCode === 37){
        moveLeft();
    }else if(event.keyCode === 39){
        moveRight();
    }
}

function deleteRow(){
    for (var row = 19;row >=0;row--){
        var canDelete = true;
        for (var col = 0;col < 10;col++){
            if (cells[row][col].className === ""){
                canDelete = false;
            }
        }
        if (canDelete){
            for (var col = 0;col < 10;col++){
                cells[row][col].className = "";
            }
            for (var downRow=row-1;row >= 0;row--){
                for (var col = 0;col < 10;col++){
                    cells[downRow + 1][col].className = cells[downRow][col].className;
                    cells[downRow + 1][col].blockNum = cells[downRow][col].blockNum;
                    cells[downRow][col].className = "";
                    cells[downRow][col].blockNum = null;
                }
            }
        }
    }
}

var fallingBlockNum = 0;
function generateBlock(){
    //ランダムにブロックを配置
    //1.ブロックパターンからランダムに一つのパターンを選ぶ
    var keys = Object.keys(blocks);
    var nextBlockKey = keys[Math.floor(Math.random()*keys.length)];
    var nextBlock = blocks[nextBlockKey];
    var nextFallingBlockNum = fallingBlockNum + 1;
    //2.選んだパターンを元にブロックを配置する
    var pattern = nextBlock.pattern;
    for (var row=0;row<pattern.length;row++){
        for (var col=0;col<pattern[row].length;col++){
            if(pattern[row][col]){
                cells[row][col+3].className=nextBlock.class;
                cells[row][col+3].blockNum=nextFallingBlockNum;
            }
        }
    }
    //3.落下中のブロックがあるとする
    isFalling=true;
    fallingBlockNum = nextFallingBlockNum;
}

function moveRight(){
    for (var row=0;row < 20;row++){
        for (var col = 9;col >=0;col--){
            if(cells[row][col].blockNum === fallingBlockNum){
                cells[row][col+1].className = cells[row][col].className;
                cells[row][col+1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}

function moveLeft(){
    for (var row=0;row <20;row++){
        for (var col = 0;col <10;col++){
            if(cells[row][col].blockNum === fallingBlockNum){
                cells[row][col-1].className = cells[row][col].className;
                cells[row][col-1].blockNum = cells[row][col].blockNum;
                cells[row][col].className = "";
                cells[row][col].blockNum = null;
            }
        }
    }
}
